/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Rodolphe Fouquet
 * ----------------------------------------------------------------------------
 */

#ifndef RESULTVIEW_HPP
#define RESULTVIEW_HPP

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>

class ResultView : public QGraphicsView {
  Q_OBJECT
public:
  explicit ResultView(QWidget *parent = 0);
public slots:
  void setResult(const QImage& image);
protected:
  virtual void wheelEvent(QWheelEvent *event);
private:
  QGraphicsPixmapItem* resultImage;

};

#endif // RESULTVIEW_HPP
