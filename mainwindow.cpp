/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Rodolphe Fouquet
 * ----------------------------------------------------------------------------
 */

#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include <QUrl>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QFileInfo>
#include <QImageReader>
#include <QImage>
#include <QTableWidgetItem>
#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow) {
  ui->setupUi(this);
  setAcceptDrops(true);
  stitchButton = ui->buttonBox->addButton(tr("&Stitch"), QDialogButtonBox::ActionRole);
  saveButton = ui->buttonBox->addButton(tr("&Save"), QDialogButtonBox::ActionRole);
  clearButton = ui->buttonBox->addButton(tr("Clear"), QDialogButtonBox::ActionRole);
  ui->resultView->setAcceptDrops(true);
  ui->imageWidget->setAcceptDrops(true);
  ui->imageWidget->setColumnCount(2);
  ui->imageWidget->setHorizontalHeaderLabels(QStringList() << tr("Path") << tr("Thumbnail"));
  ui->imageWidget->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
  ui->imageWidget->verticalHeader()->setResizeMode(QHeaderView::ResizeToContents);

  asyncTask = new QFuture<cv::Stitcher::Status>;
  asyncTaskWatcher = new QFutureWatcher<cv::Stitcher::Status>;

  connect(asyncTaskWatcher, SIGNAL(finished()), this, SLOT(stitchFinished()));
  connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
  connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(save()));
  connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(close()));
  connect(clearButton, SIGNAL(clicked()), this, SLOT(clear()));
  connect(stitchButton, SIGNAL(clicked()), this, SLOT(startStitching()));
  saveButton->setDisabled(true);
  fileFilters = tr("Png image(*.png);; Jpeg Image (*.jpeg *.jpg)");
}

MainWindow::~MainWindow() {
  delete ui;
}


void MainWindow::dropEvent(QDropEvent *e) {
  if(!e->mimeData()->hasUrls()) {
    return;
  }
  QList<QUrl> urlList = e->mimeData()->urls();
  if (!urlList.size())
    return;

  foreach(QUrl u, urlList) {
    if (QFileInfo(u.toLocalFile()).isFile()) {
      openFile(u.toLocalFile());
    }
  }
}

void MainWindow::dragMoveEvent(QDragMoveEvent *e) {
  e->accept();
}

void MainWindow::dragEnterEvent(QDragEnterEvent *e) {
  e->acceptProposedAction();
}


QImage MainWindow::putImage(const cv::Mat& mat) {
  if(mat.type()==CV_8UC1) {
    QVector<QRgb> colorTable;
    for (int i=0; i<256; i++) {
      colorTable.push_back(qRgb(i,i,i));
    }
    const uchar *qImageBuffer = (const uchar*)mat.data;
    QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_Indexed8);
    img.setColorTable(colorTable);
    return img;
  }
  if(mat.type()==CV_8UC3) {
    const uchar *qImageBuffer = (const uchar*)mat.data;
    QImage img(qImageBuffer, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
    return img.rgbSwapped();
  }
  else
  {
    qDebug() << "ERROR: Mat could not be converted to QImage.";
    return QImage();
  }
}

void MainWindow::openFile(const QString &path) {
  QTableWidgetItem *item = new QTableWidgetItem(path);
  item->setText(path);
  QTableWidgetItem *image = new QTableWidgetItem();
  item->setIcon(QIcon(path));
  ui->imageWidget->setRowCount(ui->imageWidget->rowCount() + 1);
  ui->imageWidget->setItem(ui->imageWidget->rowCount()-1, 0, item);
  image->setData(Qt::DecorationRole, QImage(path).scaledToHeight(100));
  ui->imageWidget->setItem(ui->imageWidget->rowCount()-1, 1, image);
  images.push_back( cv::imread(path.toStdString()) );
}

void MainWindow::startStitching() {
  *asyncTask = QtConcurrent::run(this,&MainWindow::stitch);
  asyncTaskWatcher->setFuture(*asyncTask);
  setDisabled(true);
}

cv::Stitcher::Status MainWindow::stitch() {
  cv::Stitcher stitcher = cv::Stitcher::createDefault(ui->gpuCheckBox->isChecked());
  stitcher.setFeaturesFinder(new cv::detail::OrbFeaturesFinder());
  return stitcher.stitch(images, stitcherResult);
}

void MainWindow::stitchFinished() {
  setDisabled(false);
  bool success = (stitcherResult.cols > 1 && stitcherResult.rows > 1 && asyncTaskWatcher->result() == cv::Stitcher::OK);
  saveButton->setDisabled(!success);
  if(success) {
    ui->resultView->setResult(putImage(stitcherResult));
  } else {
    QMessageBox::warning(this, tr("Stitching failed"), tr("You might want to try again."));
  }
}

void MainWindow::clear() {
  for(int i = 0; i < ui->imageWidget->rowCount(); ++i) {
    for(int j = 0; j < ui->imageWidget->columnCount(); ++j) {
      delete ui->imageWidget->takeItem(i,j);
    }
  }
  ui->imageWidget->clearContents();
  ui->imageWidget->setRowCount(0);
  images.clear();
}

void MainWindow::save() {
  QString outputFile = QFileDialog::getSaveFileName(this, tr("Save your panorama")
                                                    , QDesktopServices::displayName(QDesktopServices::DocumentsLocation)
                                                    , fileFilters);
  if(outputFile.isEmpty() || outputFile.isNull()) {
    return;
  }

  cv::imwrite(outputFile.toStdString(), stitcherResult);
}

void MainWindow::on_actionOpen_triggered(){
  QStringList files = QFileDialog::getOpenFileNames(this, tr("Open your input images."), QDesktopServices::displayName(QDesktopServices::DocumentsLocation), fileFilters);
  if(files.isEmpty()) {
    return;
  }

  foreach(QString file, files) {
    openFile(file);
  }
}

