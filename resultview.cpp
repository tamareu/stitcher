/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Rodolphe Fouquet
 * ----------------------------------------------------------------------------
 */

#include "resultview.hpp"
#include <QWheelEvent>
ResultView::ResultView(QWidget *parent) :
  QGraphicsView(parent), resultImage(NULL) {
  setScene(new QGraphicsScene(this));
  scene()->setSceneRect(QRectF(0,0,640,480));
  //Use ScrollHand Drag Mode to enable Panning
  setDragMode(ScrollHandDrag);
}

void ResultView::setResult(const QImage& image) {
  delete resultImage;

  scene()->setSceneRect(QRectF(0,0, image.width(), image.height()));
  resultImage = scene()->addPixmap(QPixmap::fromImage(image));
  fitInView(resultImage, Qt::KeepAspectRatio);
  centerOn(resultImage);

}

void ResultView::wheelEvent(QWheelEvent *event) {
  setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

  // Scale the view / do the zoom
  double scaleFactor = 1.15;
  if(event->delta() > 0) {
    // Zoom in
    scale(scaleFactor, scaleFactor);
  } else {
    // Zooming out
    scale(1.0 / scaleFactor, 1.0 / scaleFactor);
  }
}
