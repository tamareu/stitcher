/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Rodolphe Fouquet
 * ----------------------------------------------------------------------------
 */

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QPushButton>
#include <QGraphicsPixmapItem>
#include <QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>

#include <opencv2/opencv.hpp>
#include <opencv2/stitching/stitcher.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
private slots:
  void startStitching();
  cv::Stitcher::Status stitch();
  void stitchFinished();
  void clear();
  void on_actionOpen_triggered();
  void save();

protected:
  /**
   * Drag and drop support.
   */
  virtual void dropEvent(QDropEvent *e);
  virtual void dragMoveEvent(QDragMoveEvent *e);
  virtual void dragEnterEvent(QDragEnterEvent *e);
private:
  QImage putImage(const cv::Mat& mat);
  void openFile(const QString &path);

  Ui::MainWindow *ui;
  QPushButton *stitchButton;
  QPushButton *saveButton;
  QPushButton *clearButton;

  std::vector<cv::Mat> images;
  cv::Mat stitcherResult;
  QFuture<cv::Stitcher::Status> * asyncTask;
  QFutureWatcher<cv::Stitcher::Status> * asyncTaskWatcher;
  QString fileFilters;
};

#endif // MAINWINDOW_HPP
