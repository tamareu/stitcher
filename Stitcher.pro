#-------------------------------------------------
#
# Project created by QtCreator 2014-02-07T16:56:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Stitcher
TEMPLATE = app

DESTDIR = $$_PRO_FILE_PWD_/bin
OBJECTS_DIR = $$_PRO_FILE_PWD_/obj
UI_HEADERS_DIR = $$_PRO_FILE_PWD_/uih
UI_DIR = $$_PRO_FILE_PWD_/ui


SOURCES +=  main.cpp\
            mainwindow.cpp \
            resultview.cpp

HEADERS  += mainwindow.hpp \
            resultview.hpp

FORMS    += mainwindow.ui

INCLUDEPATH += $(OPENCV_PATH)/include

LIBS += -L$(OPENCV_PATH)/x86/vc10/lib  -L$(OPENCV_PATH)/x86/vc10/bin  \
    -lopencv_core248        \
    -lopencv_highgui248     \
    -lopencv_imgproc248     \
    -lopencv_features2d248  \
    -lopencv_stitching248   \
    -lopencv_gpu248
